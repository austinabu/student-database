package com.company;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.Scanner;





public class Student {



        private String first_name;
        private String last_name;
        private String grade_Year;
        private String id;
        private String courses = null;
        private int tuition_balance;
        private int cost_of_courses = 300;

//      Default I number
        private static int Inumber = 1001;

        // constructor: prompt user to enter student's name and year
        public Student(){
                Scanner in = new Scanner(System.in);
                System.out.print("Enter student first name: ");
                this.first_name = in.nextLine();

                System.out.print("Enter student last name: ");
                this.last_name = in.nextLine();

                System.out.print("1 -Freshmen\n2- Sophmore\n3 - junior\n4 -Senior\nEnter Enter student class level: ");
                this.grade_Year = in.nextLine();
                setid();

        }


        //Generate an Inumber

        private void setid(){
//                Grade level and Inumber
                Inumber++;
                this.id =  grade_Year + " " + Inumber;
        }

        //Enroll in courses
        public void enroll(){
        //                Get inside a loop, user hits Q
                do {
                        System.out.println("Enter course to enroll (press Q or q to quit): ");
                        Scanner in = new Scanner(System.in);
                        String course = in.nextLine().toUpperCase();
                        if (!course.equals("Q")) {
                                courses = courses + "\n " + course;
                                tuition_balance = tuition_balance + cost_of_courses;
                        } else {
                                break;
                        }
                }while(1 != 0);


        }

//        View balance

        public void view_Balance(){
                System.out.println("Your balance due is: $" + tuition_balance);
        }

        //pay tuition
        public void pay_tuition(){
                view_Balance();
                System.out.print("Enter your payment: $");
                Scanner in = new Scanner(System.in);
                int payment = in.nextInt();
                tuition_balance = tuition_balance - payment;
                System.out.println("Thank you for your payment of $" + payment + "\n");
                if (tuition_balance < 1){
                        System.out.println("You have " + (tuition_balance * -1) + " In your account");
                }
                else {
                        System.out.println("You have " + tuition_balance + " amount due \n");
                }
        }


        //show status
        public String toString(){
                return "\nName: " + first_name + " " + last_name +
                        "\nGrade level: " + grade_Year +
                        "\nStudent Id: " + id +
                        "\nCourses Enrolled: " + courses +
                        "\nBalance: $" + tuition_balance;

        }



        public void save_to_data_base(){



                try {
                        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_dbase", "root", "speedupNOW@1");
                        Statement statement = connection.createStatement();

                        PreparedStatement posted = connection.prepareStatement("INSERT INTO student(first_name, last_name, courses, grade_Year, tuition_balance) VALUES('"+first_name+"' , '"+last_name+"', '"+courses+"','"+grade_Year+"', '"+tuition_balance+"')");
                        posted.executeUpdate();


                        ResultSet resultSet = statement.executeQuery("select * from student");



                } catch (Exception e) {
                        e.printStackTrace();
                }
        }

}
